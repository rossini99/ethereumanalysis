class Node:
    def __init__(self, address,n_key, address_type ):
        self.address = address # indirizzo nodo che è anche la chiave 
        self.n_key=n_key
        self.address_type = address_type
        self.indegree=0
        self.outdegree=0
        ##TOKEN##
        self.token_transfer= [] # (to_node,contract_type,amount,token_name,TIME_STAMP)
      
        
        ##CONTRACT##
        self.contract_transfer=[]# (to_node,trace_type,amount,TIME_STAMP)
     
        ##TRACE##
        self.trace_interacts_with=[]#(to_node,interacts_type,TIME_STAMP) 
      
        
        ##TRANSFERS
        self.transfers_interacts_with=[]#(to_node,trace_type,amount,TIME_STAMP)
      
     