#dal file transactions-2016-07-09 uso gli indirizzi from to per creare gli archi e aggiungo
#tali indirizzi nel file degli hash denotandoli con un intero univoco 
#  -Nel file degli hash delle transazioni , uso l'api di eterscan per aggiungere a ciascun indirizzo
#   il bilancio attuale in quel momento (indirizzo,intero_univoco_del_nodo,balance)
#  -Nel file del grafo ho invece (intero_univoco_del_nodo1, intero_univoco_del_nodo2, valore_trasferito)

import csv
import sys

sys.path.append('./Tirocinio-2021')
import Node
def transactions_Network(n,nodes,date,traces_table,transactions_table,final_graph):
 maxInt = sys.maxsize
 from_wei_to_ether= 1000000000000000000
 value=0
 fieldnames = ['Src', 'Dst']
#tryyyyy
 while True:
    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)
        
 path ="grafo-transazioni/"
 transHash={}
 #with open(path+"traces-2016-07-09", "r",  newline ='') as tFile:
    #  reader = csv.reader(tFile)
            
 for rows in traces_table:
            transHash.update({rows[0]:rows[7]})
 
 count= 0
 ncount=0
# with open(g, "r") as gFile:
  #  with open(path+"transactions_hash.csv", "w", newline ='') as hfile:
 with open(path+"transactions_graph"+"-"+date+".csv", "w",  newline ='') as tfile:
     with open( "embGraph"+"-"+date+".csv", "a+", newline ='') as t2file:
            writer = csv.writer(tfile)
         #   writer2 = csv.DictWriter(t2file, fieldnames=fieldnames)

          #  reader = csv.reader(gFile)
          
   
            for rows in transactions_table:
                 
                  
                   if ((rows[3] is not None) and (rows[4] is not None)):
                    
                    if nodes.get(rows[3]) is None:
                        nodes.update({rows[3] : "A"+str(n)})
                        n+=1
                        ncount+=1
                    if nodes.get(rows[4]) is None:
                        nodes.update({rows[4]:"A"+str(n)})
                        n+=1
                        ncount+=1
                    
                    if rows[5]!=0 :                         # conversione Wei in ether
                        value=int(rows[5])/from_wei_to_ether
                    TIME_STAMP=str(rows[14])
                    writer.writerow([" < < "+nodes[rows[3]]+" "," :interacts_with "," " +nodes[rows[4]]+" > " ," :trace_type "," "+transHash.get(rows[0])+" ", " "+TIME_STAMP+" ", " > "])# il dizionario che contiene i tipi della transazione lo uso per estrapolarmi tale info
                    writer.writerow([" < < "+nodes[rows[3]]+" "," :interacts_with "," " +nodes[rows[4]]+" > " ," :amount "," "+ str(value)+" ", " "+TIME_STAMP+" ", " > "])
            
              #      writer2.writerow({'Src': nodes[rows[3]][1:], 'Dst': nodes[rows[4]][1:]})
                    count +=1
                   
                    
                    if ((final_graph.get(rows[3]) is  None) ):    # se il nodo from_address non è presente nel final_graph aggiungi i nodi from e to nel grafo
                       
                                 final_graph.update({ rows[3] :Node.Node(rows[3],nodes[rows[3]],nodes[rows[3]][0])})
                                 final_graph.get(rows[3]).transfers_interacts_with.append(( nodes[rows[4]],transHash.get(rows[0]),str(value),TIME_STAMP) )
                                 if ((final_graph.get(rows[4]) is  None) ):
                                   final_graph.update({ rows[4] :Node.Node(rows[4],nodes[rows[4]],nodes[rows[4]][0])})
                    else :  # se il nodo from_address è già presente nel final_graph aggiorna i nodi
                                 final_graph.get(rows[3]).transfers_interacts_with.append(( nodes[rows[4]],transHash.get(rows[0]),str(value),TIME_STAMP))
                                 final_graph.get(rows[3]).outdegree+=1
                                 if ((final_graph.get(rows[4]) is  None) ):
                                   final_graph.update({ rows[4] :Node.Node(rows[4],nodes[rows[4]],nodes[rows[4]][0])})
                                   final_graph.get(rows[4]).indegree+=1
                    
                    value=0
            print("numero interazioni nodi del grafo delle transazioni "+str(ncount)) 
            return n
            
         