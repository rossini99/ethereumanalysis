# mi scorro il file dei contratti e aggiungo nel dizionario contracts la coppia indirizzo : "nulla"

# mi scorro il file delle tracce e controllo gli indirizzi to_adress from_address se sono stati già
# inseriti nel dizionario contracts, in caso positivo aggiungo tali indirizzi nel dizionario dei nodi
# dandogli poi un indice e aggiungendoli nel file dei nodi e nel file del grafo



#Il grafo dei contratti avrà una quadrupla (intero_univoco_nodo1,intero_univoco_nodo2,tipo_arco,valore)

# Quindi in ogni arco (vi, vj,tipo_arco,valore), sia vj che vi sono nodi che rappresentano smart contract.
# Vi saranno quattro tipi di arco: create, implica la creazione di nuovi contratti intelligenti; kill, che
# implica la distruzione di uno smart contract; call, rappresenta un trasferimento di Ether; 
# daofork, cioè archi per cui si è verificata una hard fork nella blockchain

#nota : i contratti che si trovano nel file degli hash sono tutti i contratti che vengono "utilizzati"
import csv
import sys
import datetime
import pandas as pd


sys.path.append('./Tirocinio-2021')
import Node

def contracts_Network(n,nodes,date,contracts,trace_table,final_graph):

    
 maxInt = sys.maxsize
 from_wei_to_ether= 1000000000000000000


 while True:
    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)




 a = 0



 count= 0
 path1 ="grafo-contratti/"

 value=0
 Weight=[]

 Type=set()
 TypeC = []
 fieldnames = ['Src', 'Dst']
   
 dataN = {'Type': Type}
 nn=0

 dic ={}


 with open(path1+ "contracts_graph"+"-"+date+".csv", "a+", newline ='') as tfile:
     with open("embGraph"+"-"+date+".csv", "a+", newline ='') as t2file:
        writer = csv.writer(tfile)
        writer2 = csv.DictWriter(t2file, fieldnames=fieldnames)
        for rows in trace_table:
            
            try:
                
                    # row[15]is status
                    if rows[15] == 1:
                
                        if ((rows[2] is not None) and (rows[3] is not None)):
                            if ((contracts.get(rows[2]) is not None) and ((contracts.get(rows[3])) is not None)):# se l'indirizzo from_address e to_address è presente nel dizionario dei contratti
                                count +=1 
                                if rows[4]!=0 :                         # conversione Wei in ether
                                   value=int(rows[4])/from_wei_to_ether
                                TIME_STAMP =str(rows[16])
                                
                             #   writer2.writerow({'Src': nodes[rows[2]][1:], 'Dst': nodes[rows[3]][1:]})
                               
                               


                                writer.writerow([" < < " + nodes[rows[2]] + " "  , " :contract_transfer " , " " + nodes[rows[3]] + " " + " > " , " :trace_type " , " " + rows[7] + " "," "+TIME_STAMP + " > "])
                                writer.writerow([" < < " + nodes[rows[2]] + " "  , " :contract_transfer " , " " + nodes[rows[3]] + " " + " > " , " :amount " , " " + str(value) +" "," "+TIME_STAMP + " > " ])
                                
                                
                                if ((final_graph.get(rows[2]) is  None) ):    # se il nodo from_address non è presente nel final_graph aggiungi i nodi from e to nel grafo
                      
                                 final_graph.update({ rows[2] :Node.Node(rows[2],nodes[rows[2]],nodes[rows[2]][0])})
                                 final_graph.get(rows[2]).contract_transfer.append(( nodes[rows[3]],rows[7],str(value),TIME_STAMP)) #(to_node,trace_type,amount,TIME_STAMP)
                                 if ((final_graph.get(rows[3]) is  None) ): 
                                  final_graph.update({ rows[3] :Node.Node(rows[3],nodes[rows[3]],nodes[rows[3]][0])})
                                else :  # se il nodo from_address è già presente nel final_graph aggiorna i nodi
                                 final_graph.get(rows[2]).contract_transfer.append(( nodes[rows[3]],rows[7],str(value),TIME_STAMP)) #(to_node,trace_type,amount,TIME_STAMP)
                                 final_graph.get(rows[2]).outdegree+=1
                                 if ((final_graph.get(rows[3]) is  None) ): 
                                   final_graph.update({ rows[3] :Node.Node(rows[3],nodes[rows[3]],nodes[rows[3]][0])})
                                   final_graph.get(rows[3]).indegree+=1
                                value=0
                                
            except IndexError:
                pass
            continue
 
 print("numero interazioni nodi del grafo dei contratti :"+ str(n))
 return n 