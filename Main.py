from google.cloud import bigquery
from itertools import tee
import os
from etherscan import Etherscan

os.environ["GCLOUD_PROJECT"] = "blockchain-analysis-328410"
client = bigquery.Client()
import csv
import sys
sys.path.append('./grafo-contratti')
import ContractsNetwork
sys.path.append('./grafo-token')
import TokenNetwork
sys.path.append('./grafo-transazioni')
import TransactionsNetwork
sys.path.append('./grafo-tracce')
import TracesNetwork

eth = Etherscan("4AI38GH5JUP59DIFANRC5CMB5XBM6N23CR")
global n 
n=0
from_wei_to_ether= 1000000000000000000
nodes = {}
contracts={}
contracts2={}
token={}
final_graph={}
#date="2015-12-12"
startDate = "2015-12-20"
date="2015-12-20"

print("dati estratti tra "+startDate +" e "+date)
########### QUERY  CONTRATTI CREATI FINO ALLA DATA X  #################################
QUERY = (
    'SELECT address,is_erc20,is_erc721 FROM `bigquery-public-data.crypto_ethereum.contracts` WHERE DATE(block_timestamp) >= "2015-07-30" and DATE(block_timestamp) <= "'+date+'"')
query_job = client.query(QUERY)  # API request
contracts_table = query_job.result()  # Waits for query to finish
for rows in contracts_table:
  
           
             contracts2.update({rows[0]: "sc"})
             if rows[1]==False and rows[2]== False :
                    contracts.update({rows[0]: "noerc"})
             if rows[1]==False and rows[2]== True :
                    contracts.update({rows[0]: "ERC721"})
             if rows[1]==True and rows[2]==False :
                     contracts.update({rows[0]: "ERC20"}) 
             if rows[1]==True and rows[2]==True : 
                     contracts.update({rows[0]: "ercboth"})


##########QUERY  TOKEN CREATI FINO ALLA DATA X ######################################
#address,name,total_supply 
QUERY = (
    'SELECT address, name,total_supply FROM `bigquery-public-data.crypto_ethereum.tokens`  WHERE DATE(block_timestamp) >= "2015-07-30" and DATE(block_timestamp) <= "'+date+'"')
query_job = client.query(QUERY)  # API request
token_table = query_job.result()  # Waits for query to finish 
       
for rows in token_table:
            token.update({rows[0]:list([rows[1],rows[2]])})
            



##############QUERY TOKEN TRANSFERS TABLE GIORNO X #####################
#token_address,from_address,to_address,value
QUERY = (
    'SELECT token_address,from_address,to_address,value,block_timestamp FROM `bigquery-public-data.crypto_ethereum.token_transfers`   WHERE DATE(block_timestamp) >= "'+startDate+'" and DATE(block_timestamp) <=  "'+date+'"')
query_job = client.query(QUERY)  # API request
token_transfers_table = query_job.result()  # Waits for query to finish

#############QUERY TRACES TABLE GIORNO X##################################
#transaction_hash,transaction_index,from_address,to_address,value,input,output,trace_type,call_type,reward_type,gas,gas_used,subtraces,trace_address,error,status,block_timestamp,block_number,block_hash,trace_id
QUERY = (
    'SELECT * FROM `bigquery-public-data.crypto_ethereum.traces`  WHERE DATE(block_timestamp) >= "'+startDate+'" and DATE(block_timestamp) <=  "'+date+'"')
query_job = client.query(QUERY)  # API request
traces_table = query_job.result()  # Waits for query to finish


#############QUERY TRANSACTIONS TABLE GIORNO X##################################
# hash,nonce,transaction_index,from_address,to_address,value,gas,gas_price,input,receipt_cumulative_gas_used,receipt_gas_used,receipt_contract_address,receipt_root,receipt_status,block_timestamp,block_number,block_hash,max_fee_per_gas,max_priority_fee_per_gas,transaction_type,receipt_effective_gas_price
QUERY = (
    'SELECT * FROM `bigquery-public-data.crypto_ethereum.transactions`  '
   + ' WHERE DATE(block_timestamp) >= "'+startDate+'" and DATE(block_timestamp) <=  "'+date+'"')
query_job = client.query(QUERY)  # API request
transactions_table = query_job.result()  # Waits for query to finish






traces_table,traces_table2=tee(traces_table)
traces_table,traces_table3=tee(traces_table) ##creo 3 copie della tabella(iteratore) traces

for k in token.keys():
    nodes.update({k: "T"+str(n)})
    n+=1

for k in contracts2.keys():
    if (nodes.get(k) is None):
      nodes.update({k : "C"+str(n)})
      n+=1
    

                          


n=ContractsNetwork.contracts_Network(n,nodes,date,contracts2,traces_table,final_graph)
n=TokenNetwork.token_Network(n,nodes,date,contracts,token,token_transfers_table,final_graph) # script che va eseguito per prima visto che va ad aggiungere in nodes tutti i token e contratti creati fino alla data x 
n=TransactionsNetwork.transactions_Network(n, nodes,date,traces_table2,transactions_table,final_graph)
TracesNetwork.traces_Network(n, nodes,date,traces_table3,final_graph)

dirName = "./Hash/Hashes-"+date

if not os.path.exists(dirName):
    os.makedirs(dirName)
else:    
    pass



dirName = "./Final_graph"

if not os.path.exists(dirName):
    os.makedirs(dirName)
else:    
    pass
    

##creazione grafo finale  nella cartella /Final_graph (grafo che userò per generare il grafo degli id)
with open("test", "w+", newline ='') as testfile:
 with open("./Final_graph/final_graph"+"-"+date+".csv", "w+", newline ='') as h4file:
    hasher4 = csv.writer(h4file)
    test = csv.writer(testfile)
    for k,v in final_graph.items():
        test.writerow((k,v.indegree,v.outdegree ))
        ## TRACE 
        # Per le TRACE i record sono del tipo :
        #  < < from_address ,":interacts_with", to_address > , ":interacts_type", interacts_type , TIMESTAMP > 
        for x in range(len(v.trace_interacts_with)) :
            from_address = v.n_key
            to_address = v.trace_interacts_with[x][0]
            interacts_type = v.trace_interacts_with[x][1]
            TIME_STAMP = v.trace_interacts_with[x][2]
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" > ",":interacts_type"," "+interacts_type+" "," "+TIME_STAMP+" > "])


        ## TRANSACTIONS
        # Per le TRANSACTIONS  i record sono del tipo :
        #  < < from_address ,":interacts_with", to_address >,":trace_type", trace_type , TIMESTAMP> 
        #  < < from_address ,":interacts_with", to_address >,":amount", amount , TIMESTAMP> 
        for x in range(len(v.transfers_interacts_with)) :
            from_address = v.n_key
            to_address = v.transfers_interacts_with[x][0]
            trace_type = v.transfers_interacts_with[x][1]
            amount = v.transfers_interacts_with[x][2]
            TIME_STAMP = v.transfers_interacts_with[x][3]
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" >",":trace_type"," "+trace_type+" "," "+TIME_STAMP+" > " ])
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" >",":amount"," "+amount+" "," "+TIME_STAMP+" > " ])
 
    
        ## CONTRACTS
        # Per i CONTRACTS  i record sono del tipo : 
        #  < < from_address ,":interacts_with", to_address >,":trace_type", trace_type , TIMESTAMP> 
        #  < < from_address ,":interacts_with", to_address >,":amount", amount , TIMESTAMP> 
        for x in range(len(v.contract_transfer)) : 
            from_address =  v.n_key
            to_node = v.contract_transfer[x][0]
            trace_type = v.contract_transfer[x][1]
            amount = v.contract_transfer[x][2]
            TIME_STAMP = v.contract_transfer[x][3]
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" >",":trace_type"," "+trace_type+" "," "+TIME_STAMP+" > " ])
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" >",":amount"," "+amount+" "," "+TIME_STAMP+" > " ])
  
        ## TOKEN
        # Per i TOKEN i record sono del tipo : 
        #  < < from_address ,":interacts_with", to_address >,":contract_type", contract_type , TIMESTAMP> 
        #  < < from_address ,":interacts_with", to_address >,":amount", amount, TIMESTAMP > 
        #  < < from_address ,":interacts_with", to_address >,":named", name , TIMESTAMP > 
        for x in range(len(v.token_transfer)) :
            from_address = v.n_key
            to_node =v.token_transfer[x][0]
            contract_type = v.token_transfer[x][1]
            amount = v.token_transfer[x][2]
            token_name = v.token_transfer[x][3]
            TIME_STAMP = v.token_transfer[x][4]
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" >",":contract_type"," "+contract_type+" "," "+TIME_STAMP+" > " ])
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" >",":amount"," "+amount+" "," "+TIME_STAMP+" > " ])
            hasher4.writerow([" < < "+from_address+" ",":interacts_with"," "+to_address+" >",":named"," "+token_name+" "," "+TIME_STAMP+" > " ])



dirName = "./Graph_id/graph_id-"+date

if not os.path.exists(dirName):
    os.makedirs(dirName)
else:    
    pass

n_triple=0 # identificato univoco di tripla 
triple_dic={} # dizionario che conterrà tutte le triple (<from_address, interacts_with , to_address , TIMEPSTAMP > : n_triple )

predicate_dic={":amount":"0" ,":trace_type":"1", ":named":"2" ,":contract_type":"3",":interacts_type":"4",":dated":"5"}
n_obj=6
obj_dic={}







    ####### creazione grafo con id ###########

with open("./Final_graph/final_graph"+"-"+date+".csv", "r", newline ='') as rfile:
    with open("./Graph_id/graph_id-"+date+"/id_final_graph"+"-"+date+".csv", "w+", newline ='') as idfile:
     reader = csv.reader(rfile)
     writer = csv.writer(idfile)
     for rows in reader:
     
      # se l'oggetto della tripla del grafo finale con id non è stato ancora aggiunto nel dizionario degli oggetti aggiungilo e dagli un intero come valore 
      if obj_dic.get(rows[4].split(" ")[1])is None and rows[3]!=":amount" :
           obj_dic.update({rows[4].split(" ")[1]: n_obj})
           n_obj+=1
    
n_triple=n_obj  
with open("./Final_graph/final_graph"+"-"+date+".csv", "r", newline ='') as rfile:
    with open("./Graph_id/graph_id-"+date+"/id_final_graph"+"-"+date+".csv", "w+", newline ='') as idfile:        
     reader = csv.reader(rfile)
     writer = csv.writer(idfile)
     for rows in reader:
    #se la tripla non è stata ancora aggiunta nel dizionario delle triple aggiungila e dagli un intero come valore
      if triple_dic.get(" < "+ rows[0].split(" ")[3]+" "+ rows[1]+" "+ rows[2].split(" ")[1]+ " > ") is None :
       triple_dic.update( {" < "+ rows[0].split(" ")[3]+" "+ rows[1]+" "+ rows[2].split(" ")[1] +"> " : n_triple })
        # se il predicato è un amount l'oggetto sarà il valore dell'amount 
      if rows[3]==":amount":
         writer.writerow([n_triple,predicate_dic.get(rows[3]),rows[4].split(" ")[1]])
         writer.writerow([n_triple,"5",rows[5][:-2]])
      # se il predicato non è un amount l'oggetto sarà un intero univoco presente nel dizionario degli oggetti
      else :
         writer.writerow([n_triple,predicate_dic.get(rows[3]),obj_dic.get(rows[4].split(" ")[1])])
         writer.writerow([n_triple,"5",rows[5][:-2]])
      n_triple+=1
#file contenete gli oggetti del grafo finale con id 
with open("./Graph_id/graph_id-"+date+"/obj_id"+"-"+date+".csv", "w+", newline ='') as objf:
         writer = csv.writer(objf)
         for k,v in obj_dic.items():
             writer.writerow([k,v])
       

#file contentete i soggetti del grafo con id (triple)
with open("./Graph_id/graph_id-"+date+"/triple_id"+"-"+date+".csv", "w+", newline ='') as triplef:
         writer = csv.writer(triplef)
         for k,v in triple_dic.items():
             writer.writerow([k,v,])
 
#file contenente i predicato del grafo con id 
with open("./Graph_id/graph_id-"+date+"/predicate_id"+"-"+date+".csv", "w+", newline ='') as predicate:
        writer = csv.writer(predicate)
        for k,v in predicate_dic.items():
             writer.writerow([k,v,])
   







     ################       creazione file hash    ##################

with open("./Hash/Hashes-"+date+"/hash"+"-"+date+".csv", "w+", newline ='') as hfile:
    hasher = csv.writer(hfile)
    for k,v in nodes.items():
        if  v [0] == "C":
           hasher.writerow([" < < "+k + " ", " :associate " ," "+str(v)+" > ", " :address_type ", " contract  > " ])
        if  v [0] == "T":
           hasher.writerow([" < < "+k + " ", " :associate " ," "+str(v)+" > ", " :address_type ", " token  > " ])
        if  v [0] == "A":
           hasher.writerow([" < < "+k + " ", " :associate " ," "+str(v)+" > ", " :address_type ", " account  > " ])


with open("./Hash/Hashes-"+date+"/Token_hash"+"-"+date+".csv", "w+", newline ='') as h1file:
    hasher1 = csv.writer(h1file)
    for k,v in nodes.items():
  
       if v[0]=="T":
        total_s=eth.get_total_supply_by_contract_address(contract_address=str(k))
        if int(total_s)!=0:
           total_s=int(total_s)/from_wei_to_ether
           hasher1.writerow([" < < "+k + " ", " :associate " ," "+v+" > ", " :total_supply ", " "+str(total_s)+ " > " ])
        else :
           hasher1.writerow([" < < "+k + " ", " :associate " ," "+v+" > ", " :total_supply ", " "+str(0)+ " > " ])
with open("./Hash/Hashes-"+date+"/Contracts_hash"+"-"+date+".csv", "w+", newline ='') as h2file:
    hasher2 = csv.writer(h2file)
    for k,v in nodes.items():
       if v[0]=="C":
          hasher2.writerow([" < "+k + " ", " :associate " ," "+v+" > " ])



with open("./Hash/Hashes-"+date+"/Address_hash"+"-"+date+".csv", "w+", newline ='') as h3file:
    hasher3 = csv.writer(h3file)
    for k,v in nodes.items():
    #   if v[0]=="A":
      #  balance = eth.get_eth_balance(address=str(k))
       # if balance != 0 :
        #    balance=int(balance)/from_wei_to_ether
           # hasher3.writerow([" < < "+k + " ", " :associate " ," "+v+" > ", " :amount ", " "+str(balance)+ " > " ])
    #    else :
            hasher3.writerow([" < < "+k + " ", " :associate " ," "+v+" > ", " :amount ", " "+str(0)+ " > " ])




