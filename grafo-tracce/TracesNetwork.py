





import csv
import sys

sys.path.append('./Tirocinio-2021')
import Node

def traces_Network(n,nodes,date,trace_table,final_graph):


 count= 0
 interacts_type=""
 maxInt = sys.maxsize
 import datetime
 ncount= 0 
 fieldnames = ['Src', 'Dst']
 
 while True:
    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)
 path ="grafo-tracce/"
 
 with open(path+"traces_graph"+"-"+date+".csv", "a+", newline ='') as tfile:
    with open("embGraph"+"-"+date+".csv", "a+", newline ='') as t2file:
        writer = csv.writer(tfile)
       # writer2 = csv.DictWriter(t2file, fieldnames=fieldnames)
        for rows in trace_table:
            try:
                   
                    if(rows[15]==1):
                        
                        if ((rows[2] is not None) and (rows[3] is not None)):
                            
                            if nodes.get(rows[2]) is None:
                                nodes.update({rows[2] : "A"+str(n)})
                                n+=1
                               
                            if nodes.get(rows[3]) is None:
                                nodes.update({rows[3]:"A"+str(n)})
                                n+=1
                            ncount+=1
                            TIME_STAMP=str(rows[16])
                        #   writer2.writerow({'Src': nodes[rows[2]][1:], 'Dst': nodes[rows[3]][1:]})
                          
                            if nodes[rows[2]][0]=="C" and nodes[rows[3]][0]=="C":
                              writer.writerow([ " < < "+nodes[rows[2]]+" ", " :interacts_with " ," "+nodes[rows[3]]+" > "," :interacts_type ", " contract-contract ", " " + TIME_STAMP+ "> " ])
                              count +=1  
                              interacts_type="contract-contract"
                            if nodes[rows[2]][0]=="C" and nodes[rows[3]][0]=="A":
                              writer.writerow([ " < < "+nodes[rows[2]]+" ", " :interacts_with " ," "+nodes[rows[3]]+" > "," :interacts_type ", " contract-user ", " " + TIME_STAMP + "> " ])
                              count +=1     
                              interacts_type="contract-user"
                            if nodes[rows[2]][0]=="A" and nodes[rows[3]][0]=="C":
                              writer.writerow([ " < < "+nodes[rows[2]]+" ", " :interacts_with " ," "+nodes[rows[3]]+" > "," :interacts_type ", " invoke ", " " + TIME_STAMP + "> " ])
                              count +=1    
                              interacts_type="invoke"
                            if nodes[rows[2]][0]=="A" and nodes[rows[3]][0]=="A":
                              writer.writerow([ " < < "+nodes[rows[2]]+" ", " :interacts_with " ," "+nodes[rows[3]]+" > "," :interacts_type ", " transfer", " " + TIME_STAMP + "> " ])
                              count +=1        
                              interacts_type="transfer"
                            
                            if ((final_graph.get(rows[2]) is  None) ):    # se il nodo from_address non è presente nel final_graph aggiungi i nodi from e to nel grafo
                       
                                 final_graph.update({ rows[2] :Node.Node(rows[2],nodes[rows[2]],nodes[rows[2]][0])})
                                 final_graph.get(rows[2]).trace_interacts_with.append(( nodes[rows[3]],interacts_type,TIME_STAMP) )
                                 if ((final_graph.get(rows[3]) is  None) ):
                                  final_graph.update({ rows[3] :Node.Node(rows[3],nodes[rows[3]],nodes[rows[3]][0])})
                                 
                            else :  # se il nodo from_address è già presente nel final_graph aggiorna i nodi
                                 final_graph.get(rows[2]).trace_interacts_with.append(( nodes[rows[3]],interacts_type,TIME_STAMP) )
                                 final_graph.get(rows[2]).outdegree+=1
                                 if ((final_graph.get(rows[3]) is  None) ):
                                   final_graph.update({ rows[3] :Node.Node(rows[3],nodes[rows[3]],nodes[rows[3]][0])})
                                   final_graph.get(rows[3]).indegree+=1
                                 
            except IndexError:
                pass
            continue
        print("numero interazioni nodi del grafo delle tracce "+str(ncount))   

 


 
