# INPUT :   
#    - n, intero usato come contatore globale per dare un numero univoco a tutti gli address 
#    - nodes , dizionario dei nodi contentete {address : T/C/A + n}
#    - date , stringa che rappresenta la data utilizzata per estrarre i dati in quel giorno
#    - contracts , dizionario contentete tutti i contratti creati fino alla data passata in input
#    - token , dizionario contentente tutti i token creati fino alla data passata in input
#    - token_transfers_table , tabella estratta da gBQ dei token_transfers

# OUTPUT : 
#    - n , intero usato come contatore globale per dare un numero univoco a tutti gli address
#  
# EXEC : scorriamo tutte le line della tabella dei token_transfers e controlliamo se  
#        l'indirizzo from_address e to_address sono stati già aggiunti nei nodes , 
#        in caso negativo controlliamo anche se from_address e to_address sono già
#        presenti nel dizionario dei contratti in caso negativo tale address è un 
#        account (A) , quindi lo aggiungiamo nel dizionario dei nodi con il valore
#        ( A + n ) . 
#        Successivamente creiamo il grafo dei token con la seguente quintupla : 
#
#        (< < < < nodo1 , trasferimento_token , nodo2> , :contract_type , rc20/rc721/noerc/botherc > :amount  quantità_token_trasferiti> :named  nome_token>)
#

import csv
import sys
sys.path.append('./Tirocinio-2021')
import Node



import datetime
def token_Network(n,nodes,date,contracts,token,token_transfers_table,final_graph):
 maxInt = sys.maxsize
 
 from_wei_to_ether= 1000000000000000000
 value=0
 ncount = 0

 while True:

    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)

 path1 ="grafo-token/"
 count= 0

 fieldnames = ['Src', 'Dst']
 with open(path1+"token_graph"+"-"+date+".csv", "w",  newline ='') as tfile:
   with open("embGraph"+"-"+date+".csv", "a+", newline ='') as t2file:
            writer = csv.writer(tfile)
         #   writer2 = csv.DictWriter(t2file, fieldnames=fieldnames)
            for rows in token_transfers_table:
                    if nodes.get(rows[1]) is None: # row[1] from_address 
                      if contracts.get(rows[1] ) is None:
                        nodes.update({rows[1] : "A"+str(n)})
                        n+=1
                        ncount+=1
                     
                    if nodes.get(rows[2]) is None: # row[2] to_adress
                      if contracts.get(rows[2]) is None :
                        nodes.update({rows[2]: "A"+str(n)})
                        n+=1
                        ncount+=1
                      
                    if rows[3]!=0 :                         # conversione Wei in ether
                                   value=int(rows[3])/from_wei_to_ether
                    TIME_STAMP = str(rows[4])
                    writer.writerow([" < < " + nodes[rows[1]], " :token_transfer " , nodes[rows[2]] + " > ", " :contract_type " , contracts.get(rows[0]) ," "+TIME_STAMP+ " > "])  #aggiungo la transazione 
                    writer.writerow([" < < " + nodes[rows[1]], " :token_transfer " , nodes[rows[2]] + " > ", " :amount " , str(value)  ," "+str(rows[4]) + " > "])  
                    writer.writerow([" < < " + nodes[rows[1]], " :token_transfer " , nodes[rows[2]] + " > ", " :named " ," "+str(token.get(rows[0][0])) ," "+TIME_STAMP + " > "])  
                    count +=1
                
                 #   writer2.writerow({'Src': nodes[rows[1]][1:], 'Dst': nodes[rows[2]][1:]})

                    if ((final_graph.get(rows[1]) is  None) ):    # se il nodo from_address non è presente nel final_graph aggiungi i nodi from e to nel grafo
                       
                      final_graph.update({ rows[1] :Node.Node(rows[1],nodes[rows[1]],nodes[rows[1]][0])})
                      final_graph.get(rows[1]).token_transfer.append(( nodes[rows[2]],contracts.get(rows[0]),str(value),str(token.get(rows[0][0])),TIME_STAMP)) #(to_node,contract_type,amount,token_name,TIME_STAMP)
                      if ((final_graph.get(rows[2]) is  None) ):
                        final_graph.update({ rows[2] :Node.Node(rows[2],nodes[rows[2]],nodes[rows[2]][0])})
                    else :  # se il nodo from_address è già presente nel final_graph aggiorna i nodi
                      final_graph.get(rows[1]).token_transfer.append(( nodes[rows[2]],contracts.get(rows[0]),str(value),str(token.get(rows[0][0])),TIME_STAMP)) #(to_node,contract_type,amount,token_name,TIME_STAMP)
                      final_graph.get(rows[1]).outdegree+=1
                      if ((final_graph.get(rows[2]) is  None) ):
                          final_graph.update({ rows[2] :Node.Node(rows[2],nodes[rows[2]],nodes[rows[2]][0])})
                          final_graph.get(rows[2]).indegree+=1
                    value=0
            print("numero interazioni nodi del grafo dei token "+str(ncount)) 
            return n
           
       
           
         